const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const webpack = require('webpack');
const path = require('path');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

module.exports = merge(common, {
    devtool: 'inline-source-map',
    watch: true,
    plugins: [
        new BrowserSyncPlugin({
          host: 'localhost',
          port: 3000,
          files: ['./public/*.html'],
          server: { 
              baseDir: ['public'] 
            }
        })
      ],
    mode: 'development'
});
