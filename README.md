Simple template for developing static sites (html + sass + js).

## Installing

Clone or fork this repository

```
git clone https://gitlab.com/cjvnjde/creative_digital_agencies.git
```

## Usage

for developing
```
yarn install
yarn start
```

for building
```
yarn install
yarn build
```

After that you can change files in the folder **'/src'**. Web page will reload automatically.

## Gitlab hosting

After pushing to gitlab web page will be automatically available at ``https://username.gitlab.io/projectname``